#+title: Akib Azmain Turja

Hi there, I'm Akib Azmain Turja, and this is my personal website.
Here I write about things that are interesting to me.  My interests
change over time, and so will this website too.

* Recent Blogs

#+include: blog.org :lines "3-11"
[[file:blog.org][More...]]

* RSS Feed

RSS feed for blog is available [[file:blog.xml][here]].
