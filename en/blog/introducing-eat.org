#+title: Introducing Eat: A New Terminal Emulator for Emacs
#+date: <2022-11-25 Fri>

Several hours ago, I published [[https://codeberg.org/akib/emacs-eat][Eat on Codeberg]].  Eat stands for
"Emulate A Terminal."  As the name suggests, it's a new terminal
emulator for Emacs.

* How It Started

I have used all three terminal emulators that I know to exist.  These
are Term, Coterm and Vterm.  I don't like Term's keybindings in char
mode, and it sometimes messes up thing when in line mode.  Coterm is
great, but I'm not a Shell mode fanboy, so that also doesn't fit for
me.  Vterm is a fully-featured terminal powered by libvterm, but still
I don't like Vterm.  Perhaps that because it doesn't have a char mode
by default, or maybe something else that I don't know myself.

So I started writing Eat.  At first it was really just for fun
project.  But, as I implemented more and more things, I found that Eat
is actually a bit faster than the other terminal emulator written in
Emacs Lisp, despite I didn't wrote Eat with performance in mind.  So,
I started to take it seriously and optimized as much as I can.  I also
tried to make the terminal emulation code as much reusable as
possible.

* Features

Eat's main feature is Eshell integration.  I use Eshell as my primary
shell, so I kept the terminal emulation code reusable to implement a
terminal in Eshell.  As the terminal emulation code is shared, both
Eat's own terminal UI and Eshell integration has same features despite
the UIs being different.

To be honest, Eat doesn't have too many features at the moment, partly
because I don't need them, partly because I'm current focusing on more
automated tests instead of new features.  But Eat terminal emulator
has four key features, complete mouse support, complete clipboard
integration and decreased flickering and speed.

Just like other XTerm-like terminals, you can use mouse.  All you have
to do is use the mouse, nothing more.  Mouse supporting programs will
automatically get click, dragging and movement events, even when your
Emacs is in the terminal.

Clipboard integration is also implemented.  That means you can copy
text in your Emacs and paste it in your terminal, or vice versa.
However the program in your terminal can't get your clipboard or kill
ring content unless you yank (paste) it with Emacs, that's disabled by
default for security reasons.

Eat doesn't flicker much.  On my computer, it is faster than Vterm,
because Eat flickers way less than Vterm.  If you want to know why,
here it is: after some research, I discovered that programs send
updates to terminal in parts, which causes flickering.  Eat uses some
stupid yet effective heuristics inspired by Suckless terminal to join
the parts to show them all at once, reducing the number of redisplays.
Redisplay is an expensive process, so skipping some redisplays means
Eat gets that time to process more things.  That's why Eat is faster
that Vterm in many cases, when Vterm is wasting time doing useless
redisplays, Eat is processing more things to display all at once.

Eat is reasonably fast, faster than any other Emacs Lisp terminal
emulator.  On my computer, Eat beats native-compiled Term even without
byte compilation, and native-compiled Eat is almost five times faster
than Term.  Coterm is a great terminal in my opinion, both in terms of
speed and in terms of code-readability.  But Eat is faster than
Coterm, around 1.4 to 1.9 times faster.  Vterm's terminal emulator is
actually a C library, so it's extremely hard to even come close to its
speed.  On my computer, Vterm is around 1.5 to two times faster, but
since Vterm wastes time flickering, Eat usually faster in most
non-trivial cases.

* Installing

Eat is not available in any package archive, but if you do use Quelpa,
I have this recipe for you:

#+begin_src emacs-lisp
(quelpa '(eat :fetcher git
              :url "https://codeberg.org/akib/emacs-eat.git"
              :files ("*.el" "dir"
                      "*.info" "*.texi"
                      "*.ti" ("e" "e/*")))
#+end_src
