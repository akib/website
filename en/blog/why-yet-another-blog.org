#+title: Why Yet Another Blog?
#+date: <2022-11-08 Tue>

* There are already countless blogs, then why yet another?

A blog is a place for you to share your ideas, experience, feelings
and passion.  It allows you write freely about whatever you think.
Since the blog is on your own website, there is no restriction.  You
can optimize your website to make reading more pleasant.  Your blog
will also be indexed by search engines, so anyone can find your blog
posts just with a search.

Many people use social media to express themselves.  It's nice for
publishing some quick update, but it's not enough to express
everything.  That's because many social medias place artificial
limits, and even if there is no technical restriction, people don't
usually take the time to read your long posts.  Since social medias
are optimized for many small status updates, so reading long posts
aren't very convenient and it's very hard (if possible) to find a
particular post.  And if you use unethical social medias like Facebook
or Twitter, the algorithms may (and will) also get in your way.

So I encourage you to start your own blog and write about your
interests, just like many other bloggers.  You don't need to update
your blog regularly, just update when you have something to say and
some free time to write your thoughts.

* OK, but why a static website?  How about frameworks like WordPress?

I don't need a dynamic website.  Static websites are a lot faster than
dynamic websites.  Dynamic websites typically do a lot of processing
on every request.  This wastes a lot of power and time, and in this
world with problems like pollution, global warming and climate change,
I don't want to worsen the situation.

Static websites are easier to maintain than dynamic ones.  There is
nothing to execute, so there isn't any security problem.  I can
maintain a static website with the comfort of Emacs and Org mode, and
that's what I'm doing here right now.
