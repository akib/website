#+title: Reading Info manuals in Emacs: Learning Emacs with Emacs
#+date: <2022-11-19 Sat>

The best tutorial to learn to GNU Emacs is in Emacs itself.  It is the
GNU Emacs Manual, shipped with Emacs as an Info manual.  To read Info
manuals, there is the Info package.  And it's extremely easy to use.

Press =C-h i= or =Control-h= + =i= to start Info in Emacs.  If you
know how to use the stand-alone Info package, you'll feel at home.

If you're new to Emacs and prefer mouse over keyboard, you can use
mouse.  But I'll focus on keyboard, because that's more efficient.
I'll try to make easy it for the new Emacs users to grasp.

After you start Info, you'll see the following text at the beginning,
to help be familiar with Info:

# Is this a good idea?  verse inside quote works while exporting, but
# not properly highlighted in Emacs.

#+begin_quote
#+begin_verse
  This (the Directory node) gives a menu of major topics.
  Typing "q" exits, "H" lists all Info commands, "d" returns here,
  "h" gives a primer for first-timers,
  "mEmacs<Return>" visits the Emacs manual, etc.

  In Emacs, you can click mouse button 2 on a menu item or cross reference
  to select it.
#+end_verse
#+end_quote

As it suggests, if you're new to Info, you should definitely check out
the =h= key.  Press =h= and you'll get a nice tutorial to help you get
started.

To open a manual, press =m= and type the manual you want to open.
=TAB= can be used for completion.  Press enter when you're done.  For
example, to open the GNU Emacs Manual, press =m Emacs RET= (=m= then
type =Emacs=, followed by enter or return).

In the first node of a manual, you'll see a menu of chapters, and you
can press =m= to jump to one of them too.

Actually, you can press =m= on any node (or page) to jump to an item
in the menu.  The first node, Info director, has a menu of manual, and
you just selected one with =m=.

To scroll up, press the key =SPC=, or space.  This will scroll up, and
if you're already at the bottom, it'll take you to the beginning of
next node.  its opposite is =DEL=, or backspace.  It'll scroll down
and take you to the end of previous node if you're already at the top.

If you want to go to the beginning of next node without scrolling,
press the key =n=.  Likewise, to go to the beginning of previous node,
press =p=.

If you feel lost, you can always go to the very first node, or the
directory node, with =d=.  Or if you want to go to first node of the
current manual, try =t=.

Info also maintains a history.  You can get the history menu with =L=.
But why someone would need a menu of history just to go to the
previously viewed node, when there's a key to do that.  Try =l=, it'll
take you to the previous node you looked at.  =r= is the opposite,
it'll take you to the next node on history.  And interestingly, =l=
and =r= are placed next to each other in the Dvorak keyboard layout,
and I like it.

These are just the few keys you need to remember.  Just kidding.  Just
remember how to start Info (=C-h i= or =Control-h= + =i=), and if you
ever can't remember something, press =H=, it'll list of all available
keys.  Only these two things to remember.  And don't forget you can
also use your arrow key if you wish.

Now as you know how to read the Info manual, go and read the GNU Emacs
Manual within Emacs to learn Emacs!
